### Hi there 👋, Welcome to Excellogics Tech Solutions
![](https://excellogics.co.in)


- 🔭 We are currently working on multiple projects. 
- 📫 How to reach us: software@excellogics.co.in 


[<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/github.svg' alt='github' height='40'>](https://github.com/https://gitlab.com/excellogics)  [<img src='https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/icloud.svg' alt='website' height='40'>](https://excellogics.co.in)  

<a href='https://docs.github.com/en/developers'><img src='https://raw.githubusercontent.com/acervenky/animated-github-badges/master/assets/devbadge.gif' width='40' height='40'></a> 

